<?php

use Illuminate\Http\Request;
use App\Model\Admin\Categories;
use App\Model\Admin\ContentsModel;

use \App\Http\Controllers\Admin\SettingController;
//use Mail;
use App\Mail\SendMail;
use App\Model\Admin\Contact;
use App\Model\Admin\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
   
    'middleware' => ['cors']
], function () {
    

    Route::get('/category/list/{categoryName}', function ($categoryName) {


        $category_with_contents = Categories::with('contents')->where(['name' => $categoryName, 'status' => 'active'])->first();


        return response($category_with_contents, 200);
    });
    Route::get('/category/detail/{id}', function ($id) {


        $category_with_contents = ContentsModel::with('galleries')->where(['id' => $id, 'status' => 'active'])->first();


        return response($category_with_contents, 200);
    });
       

   // send Mail
    Route::post('/sendMessage', function (Request $request) {
        $setting=SettingController::getSetting();
        $formInput=json_decode(request()->getContent(), true);




        Mail::to($setting['owner_email'],$setting['owner_name'])->send(new SendMail($formInput));
        if (Mail::failures()) {


            return response(['error'=>'Cannot Send Message']);
        }

        if( Contact::create($formInput)){
            return response(['success'=>'Successfully Send Message']);
        }else {
            return response(['error'=>'Cannot Send Message']);
        }
    });
    // get Product

     Route::get('/products', function () {
       $products=Categories::with('contents')->where(['name' => 'product', 'status' => 'active'])->first();

        return response($products, 200);
    });
     // product gallery


    // aapp and user setting
    Route::get('/aboutUs', function () {
        $data['setting'] = SettingController::getSetting();
          $data['user'] = User::find(1)->first();
        return response($data, 200);
    });
     
    // get Categories image and Name
    Route::get('/contentImagesAndName', function () {
        $category=Categories::where('name','products')->first();


        $contentImagesAndName = ContentsModel::where(['status' => 'active','categoryId'=>$category->id])->get();


        return response($contentImagesAndName, 200);
    });

    Route::get('/contentGallery/{id}', function ($id) {
      

        $list_gallery = ContentsModel::with('galleries')->where(['status' => 'active','id'=>$id])->first();


        return response($list_gallery, 200);
    });

});





