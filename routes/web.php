<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth Routing
Route::group([
    'namespace'=>'Admin'
],function(){
    Auth::routes();
});



//Admin Routing
Route::group([
    'namespace' => 'Admin',
    'prefix' => '/control_panel',
    'middleware' => ['auth']
], function () {
    Route::get('/', 'HomeController@index')->name('control_panel');
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/update/profile', 'ProfileController@update')->name('update.profile');

    // Route::get('/fileManager', 'FileManagerController@index')->name('fileManager.index');
    Route::resource('/menu', 'MenuController');
    Route::post('/menu/saveMenu/', 'MenuController@saveMenu')->name('menu.saveMenu');
    Route::get('/menu/getUrl/{id}', 'MenuController@getUrl')->name('menu.getUrl');


    Route::resource('/setting','SettingController');
    Route::get('getSetting', 'SettingController@getSetting')->name('setting.getSetting');


    Route::resource('/categories', 'CategoriesController');
    Route::post('/categories/saveCategories/', 'CategoriesController@saveCategories')->name('categories.saveCategories');

    Route::resource('/contents', 'ContentsController');
    Route::get('/getDataTable', 'ContentsController@getDataTable')->name('contents.getDataTable');

    Route::resource('/gallery', 'GalleryController');
    Route::resource('/contact', 'ContactController');

});


//Web routing
Route::group([
    'namespace' => 'Web'

], function () {
    Route::group([
        'prefix' => 'category_type'
    ], function () {
        Route::get('/list/{category_type}', 'WebController@list_category_type');
        Route::get('/detail/{category_type}/{id}', 'WebController@detail_category_type')->name('detail.category_type');
    });
    Route::get('/', 'WebController@index');
    Route::get('/contact', 'WebController@contact');
    Route::get('/aboutus', 'WebController@aboutus');
     Route::post('/sendMail', 'WebController@sendMail');
     Route::get('/gallery', 'WebController@gallery');
      Route::get('/list_gallery/{id}', 'WebController@list_gallery')->name('list_gallery');
});
