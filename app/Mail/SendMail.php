<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $postData;
    public function __construct($requestData)
    {
        $this->postData['data']=$requestData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email=$this->postData['data']['email'];
        $name=$this->postData['data']['name'];
        $subject = 'Customer Message';
        $this->postData['data']['subject']=$subject;
        return $this->view('web.includes.mail',$this->postData)

                    ->from($email,$name)
                    ->subject($subject);

    }
}
