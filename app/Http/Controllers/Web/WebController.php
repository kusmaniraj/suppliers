<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BuildTree;
use App\Http\Controllers\Admin\SettingController;
use App\Model\Admin\ContentsModel;
use App\Model\Admin\Categories;
use App\Model\Admin\GalleryModel;
use App\Model\Admin\MenuModel;
use App\Model\Admin\Contact;
use Validator;
use Session;
use Mail;
use App\Mail\SendMail;
class WebController extends Controller
{
    protected $data;
    public function __construct()
    {
        $this->data['setting']=SettingController::getSetting();
        $this->data['sliders']=ContentsModel::where(['slider'=>'yes','status'=>'active'])->get();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id','position','url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $tree=new BuildTree;
        $this->data['menus'] = $tree->buildTree($menuLists);
        $categoriesPeopleReview=Categories::where('name','People Reviews')->first();
         if(isset($categoriesPeopleReview)){
            $this->data['list_PeopleReviews']=Categories::find($categoriesPeopleReview['id'])->contents()->where(['status'=>'active'])->get();
         }


        
        

    }
    public function index(){
       
       
       $this->data['title']='Home';
        $categoriesFacility=Categories::where('name','Facilities')->first();
         if(isset($categoriesFacility)){
            $this->data['list_facilitiess']=Categories::find($categoriesFacility['id'])->contents()->where(['status'=>'active'])->get();
         }
          $categoriesProduct=Categories::where('name','Products')->first();
         if(isset($categoriesProduct)){
            $this->data['list_products']=Categories::find($categoriesProduct['id'])->contents()->where(['status'=>'active'])->orderByRaw('RAND()')->get();
         }

         $categoriesOffer=Categories::where('name','Offers')->first();
         if(isset($categoriesOffer)){
            $this->data['list_offers']=Categories::find($categoriesOffer['id'])->contents()->where(['status'=>'active'])->orderByRaw('RAND()')->take(6)->get();
         }

          $categoriesTestimonial=Categories::where('name','Testimonials')->first();
         if(isset($categoriesTestimonial)){
            $this->data['list_testimonials']=Categories::find($categoriesTestimonial['id'])->contents()->where(['status'=>'active'])->get();
         }

         $categoriesPeopleReview=Categories::where('name','People Reviews')->first();
         if(isset($categoriesPeopleReview)){
            $this->data['list_PeopleReviews']=Categories::find($categoriesPeopleReview['id'])->contents()->where(['status'=>'active'])->get();
         }
        
        
        return view('web.index',$this->data);
    }
    public function contact(){

        $this->data['title']='Contact';
        $this->data['menuImg']=$this->menusDeatails('सम्पर्क');
        return view('web/contact', $this->data);
    }

    public function aboutus(){

        $this->data['title']='About Us';
        $this->data['menuImg']=$this->menusDeatails('हाम्रो बिषयमा');
        return view('web/contact', $this->data);
    }
    public function list_category_type($category_type){
        $category_name=$this->checkCategoryType($category_type);
         $this->data['title']=$category_name;

        $this->data['menuImg']=$this->menusDeatails($category_name);


        $categoriesDetails=Categories::where('name',$category_type)->first();
       

        if(isset($categoriesDetails)){
             $this->data['category_name']=$categoriesDetails['name'];
             $this->data['list_categories']=Categories::find($categoriesDetails['id'])->contents()->where(['status'=>'active'])->paginate(4);
        }
       
        
        return view('web/category/list',$this->data);
    }
    public function detail_category_type($categoryId,$id){
        

        $categoriesDetails=Categories::where('id',$categoryId)->first();
        $category_name=$this->checkCategoryType($categoriesDetails['name']);
      
         $this->data['menuImg']=$this->menusDeatails($category_name);
        if(isset($categoryId)){
             $this->data['category_name']=$categoriesDetails['name'];
             $this->data['list_categories']=Categories::find($categoryId)->contents()->where(['status'=>'active'])->get();

        }
        $this->data['content_images']=GalleryModel::where(['ContentId'=>$id])->orderByRaw('RAND()')->take(3)->get();
        $this->data['detail_content']=ContentsModel::where(['id'=>$id])->first();
          $this->data['title']=$categoriesDetails['name'].'|'.$this->data['detail_content']['name'];
        $this->data['detail_category']=$categoriesDetails;
        
       
        return view('web/category/detail', $this->data);
    }

    public function sendMail(Request $request){
      $setting=SettingController::getSetting();
       $formInput['created_at']=new \DateTime();

      $formInput=$request->all();
      Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:255',
            'message'=>'required',

        ])->validate();

        
         $sendMail=Mail::to($setting['owner_email'],$setting['owner_name'])->send(new SendMail($formInput));
         if (Mail::failures()) {
            
            Session::flash('error', 'Error to send Message');
             return redirect()->back();
           }

         if( Contact::create($formInput)){
          Session::flash('success', 'Message has been Sent');
         }else {
            Session::flash('error', 'Error to send Message');
        }
        
        return redirect()->back();

    }

    public function gallery(){

          $this->data['title']='Gallery';
        
         $this->data['menuImg']=$this->menusDeatails('ग्यालरी');
         $this->data['contents']=ContentsModel::with('category')->whereHas('category', function ($query) {
             $query->where('name','Products' );
         })->where(['status'=>'active'])->paginate(4);
        return view('web.gallery',$this->data);
    }
     public function list_gallery($ContentId){

       
        $this->data['menuImg']=$this->menusDeatails('ग्यालरी');
        $this->data['category_name']=Categories::select('name')->where('id',$ContentId)->first();
         $this->data['title']='Gallery'.'|'.$this->data['category_name']['name'];
        $this->data['list_galleries']=ContentsModel::where('status','active')->whereHas('category', function ($query) {
            $query->where('name','Products' );
        })->orderByRaw('RAND()')->take(10)->get();

         $this->data['galleries']=GalleryModel::where('ContentId',$ContentId)->paginate(8);
        return view('web.gallery',$this->data);
    }
    public function menusDeatails($menuName){
         return MenuModel::where(['name'=>$menuName])->first();
    }

    public function checkCategoryType($type){
        if(ucfirst($type)=='Notices'){
            $category_name='सूचना';
        }elseif(ucfirst($type)=='Offers'){

            $category_name='प्रस्ताव';

        }elseif(ucfirst($type)=='Products'){
            $category_name='सामाग्री';
        }else{
            $category_name="";
        }

        return $category_name;
    }



   

}
