<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\SettingController;

class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    


   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/control_panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $data;
   
    public function __construct()
     
    {
        $this->data['getSetting']=SettingController::getSetting();
        $this->middleware('guest')->except('logout');
    }
     public function showLoginForm()
    {
        return view('admin.auth.login',$this->data);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/control_panel');
    }

    

}
