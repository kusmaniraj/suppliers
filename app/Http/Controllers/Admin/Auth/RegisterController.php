<?php

namespace App\Http\Controllers\Admin\Auth;


use App\Model\Admin\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Admin\SettingController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/control_panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $data;
   
    public function __construct()
    {
        $this->middleware('guest');
         $this->data['getSetting']=SettingController::getSetting();
    }
     public function showRegistrationForm()
    {
        return view('admin.auth.register',$this->data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender'=>'required',
           
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {

        $user_data = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'gender'=>$data['gender'],
            'password' => bcrypt($data['password']),
        ]);
       
       return $user_data;
    }
}
