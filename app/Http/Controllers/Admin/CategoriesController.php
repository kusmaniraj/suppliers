<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Categories;
use App\Model\Admin\ContentsModel;
use App\Http\Controllers\Admin\SettingController;
use DB;
use App\Http\Controllers\BuildTree;
use Validator;
use Illuminate\Support\Facades\Session;
use Image;
use Input;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $data;
    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
        $categoriesList = Categories::select('id', 'name as text', 'parent_id','position')->orderBy('position')->get()->toArray();

        $tree= new BuildTree;
        $this->data['categories'] = $tree->buildTree($categoriesList);
         $this->data['title']='Categories';

    }

    public function index()
    {
    return view('admin/categories',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formInput = $request->all();
        $formInput['featuredImg']=$this->imgUpload($request->file('featuredImg'));
       $formInput['created_at']=new \DateTime();
        $formInput['isDeletable']='yes';
         unset($formInput['oldFeaturedImg']);

        Validator::make($request->all(), [
            'name' => 'required|max:20'
            ,['name.required'=>'*Please Input Menu Name*']

        ])->validate();


        if (Categories::insert($formInput)) {
            Session::flash('success', 'Add Data Successfully');
            return redirect('control_panel/categories');


        } else {
            echo "error";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['category_detail'] = Categories::find($id);
        return view("admin/categories", $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();
        $image_name=$formData['oldFeaturedImg'];
        $formData['isDeletable']='yes';

        Validator::make($request->all(), [
            'name' => 'required|max:20'
            ,['name.required'=>'*Please Input Category Name*']

        ])->validate();
        if (empty($formData['featuredImg'])) {

            $formData['featuredImg'] = $formData['oldFeaturedImg'];
        }else{
            $formData['featuredImg']=$this->imgUpload($request->file('featuredImg'));
             if($image_name){
                 $this->deleteImg($image_name);
             }
        }



        if (Categories::find($id)->update($formData)) {
            
            Session::flash('success', 'Update Categories Successfully');

        } else {
            Session::flash('error', 'Cannot Updated Categories Successfully');

        }
        return redirect()->route('categories.edit', $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $image_name=Categories::select('featuredImg')->where('id',$id)->first();  
        if(count(ContentsModel::where('categoryId',$id)->get())==null){
            if (Categories::find($id)->delete()) {
                if($image_name){
                 $this->deleteImg($image_name['featuredImg']);
                }
            Session::flash('success', 'Delete Categories Successfully');
            } 
         }else{
                Session::flash('error', 'Current Category Cannot be Deleted');
            }
        

    }

    


    public function saveCategories( Request $request){
        $categories = $request->input('treeExport');

        foreach ($categories as $key => $category) {

            $category['position'] = $key;
            Categories::where('id',$category['id'])->update($category);

        }
    }

    public function imgUpload($image){
         if($image){
                $image_name= time().'.png';
               

                 $path = public_path() . "/files/1/" . $image_name;
                Image::make($image)->resize(1366,281)->save($path);
                $path2 = public_path() . "/files/1/thumbs/" . $image_name;
                Image::make($image)->resize(200,200)->save($path2);
                return $image_name;


      
       

       }
    }
    public function deleteImg($image_name){
             $pathImg1=public_path('files/1/'.$image_name);
             if(file_exists($pathImg1) ){
                 @unlink($pathImg1);
             }
              $pathImg2=public_path('files/1/thumbs/'.$image_name);
           
             if(file_exists($pathImg2) ){
                 @unlink($pathImg2);
             }
             return true;
    }
}
