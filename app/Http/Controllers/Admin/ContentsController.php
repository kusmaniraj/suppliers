<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContentRequest;
use App\Model\Admin\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\ContentsModel;
use App\Http\Controllers\Admin\SettingController;
use DB;
use Validator;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use Image;

class ContentsController extends Controller
{

    private $data;
    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
        $this->data['categories']=Categories::select('name','id','status')->where(['status' => 'active'])->get() ;
         $this->data['title']='Contents';


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['listContents']=ContentsModel::get();

        return view('admin/contents/list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



        return view('admin/contents/form',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContentRequest $request)
    {
        $formInput = $request->all();
       if($this->countSlider($formInput['slider'])==false) {
        Session::flash('errorSlider', '*FeaturedImg couldnot be more than 5*');
         return redirect()->back();
       }
       
        $formInput['featuredImg']=$this->imgUpload($request->file('featuredImg'));
       
        $formInput['created_at']=new \DateTime();
       

        unset($formInput['oldFeaturedImg']);




        if (ContentsModel::insert($formInput)) {
            Session::flash('success', 'Add Data Successfully');
            return redirect('control_panel/contents');


        } else {
            Session::flash('error', 'Erro to Add Data ');

        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['contents'] = ContentsModel::find($id);
        return view("admin/contents/form", $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();
        $image_name=$formData['oldFeaturedImg'];
        if($this->countSlider($formData['slider'])==false) {
        Session::flash('errorSlider', '*FeaturedImg couldnot be more than 5*');
         return redirect()->back();
       }


        if (empty($request->file('featuredImg'))) {

            $formData['featuredImg'] = $formData['oldFeaturedImg'];
        }else{
             $formData['featuredImg']=$this->imgUpload($request->file('featuredImg'));
            if($image_name){
                    $this->deleteImg($image_name);
                }
        }

        unset($formData['oldFeaturedImg']);
        if (ContentsModel::find($id)->update($formData)) {
            
            Session::flash('success', 'Update contents Successfully');
            return redirect()->route('contents.index');

        } else {
            Session::flash('error', 'Cannot Updated contents Successfully');
            return redirect()->route('contents.edit', $id);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image_name=ContentsModel::select('featuredImg')->where('id',$id)->first();
        

        if (ContentsModel::find($id)->delete()) {
            if($image_name){

             $this->deleteImg($image_name['featuredImg']);
            }
            Session::flash('success', 'Delete contents Successfully');


        } else {
            Session::flash('error', 'Cannot Delete contents Successfully');
        }
        return redirect()->route('contents.index');
    }
    public function countSlider($slider){
        if($slider=='no'){
             return true;
        }
        $countSlider=count(ContentsModel::where('slider','yes')->get());
        if($countSlider>=5){
              return false;

        }else{
            return true;
        }
    }

public function imgUpload($image){
         if($image){
                $image_name= time().'.png';
                 $path = public_path() . "/files/1/" . $image_name;
                Image::make($image)->resize(1366,768)->save($path);
                $path2 = public_path() . "/files/1/thumbs/" . $image_name;
                Image::make($image)->resize(200,200)->save($path2);
                return $image_name;


      
       

       }
}
public function deleteImg($image_name){
             $pathImg1=public_path('files/1/'.$image_name);
             if(file_exists($pathImg1) ){
                 @unlink($pathImg1);
             }
              $pathImg2=public_path('files/1/thumbs/'.$image_name);
           
             if(file_exists($pathImg2) ){
                 @unlink($pathImg2);
             }
             return true;
}

}
