<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Contact;
use App\Http\Controllers\Admin\SettingController;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $data;
    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
        $this->data['title']='Contact';


    }

    public function index()
    {
        $this->data['customer_messages']=Contact::get();
        return view('admin.contact',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $response=Contact::where('id',$id)->first();
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData=$request->except('_method');
        $result=Contact::where('id',$id)->update($formData);
        if ($result) {
            Session::flash('success', 'Update Message as Read Successfully');


        } else {
            Contact::flash('error', 'Cannot update Message as Unread Successfully');
        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if (Contact::find($id)->delete()) {
            Session::flash('success', 'Delete Message Successfully');


        } else {
            Contact::flash('error', 'Cannot Delete Message Successfully');
        }
        return redirect()->back();
    }
}
