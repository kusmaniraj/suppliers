<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\ContentsModel;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\GalleryModel;
use Image;
use DB;
use Validator;
use Illuminate\Support\Facades\Session;
use File;

class GalleryController extends Controller
{
    private $data;
    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
         $this->data['title']='Gallery';



    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formInput=$request->all();
        $formInput['created_at']=new \DateTime();
        $isValid=Validator::make($request->all(), [
            'images' => 'required|',

        ]);
        if ($isValid->fails()) {
            return redirect('control_panel/gallery/'.$formInput['contentId'])
                ->withErrors($isValid)
                ->withInput();
        }

        $contentImages=$request->file();
        if(isset($contentImages['images'])){
            foreach ($contentImages['images'] as $image){

                $formInput['images']=$this->uploadImage($image);

                GalleryModel::insert($formInput);
            }
            Session::flash('success', 'Uploaded Successfully');
            return redirect('control_panel/gallery/'.$formInput['contentId']);

        }










    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['contentDetails']=ContentsModel::find($id);
        $this->data['images']=GalleryModel::where('contentId',$id)->paginate(4);

        return view('admin/gallery/list',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contentDetails=GalleryModel::find($id);
        $contentId=$contentDetails['contentId'];
        $image=$contentDetails['images'];
        $file_path=public_path($image);

        if (GalleryModel::find($id)->delete()) {
            if(file_exists($file_path)){
                @unlink($file_path);
            }
            Session::flash('success', 'Delete contents Successfully');


        } else {
            Session::flash('error', 'Cannot Delete Image ');
        }
        return redirect()->route('gallery.show',$contentId);
    }
    public function uploadImage($contentImage)
    {
        $path=public_path('files/1/Gallery/Content/');
        if (!file_exists($path)) {
        $result = File::makeDirectory($path, 0775, true);
        }
        $filename =$contentImage->getClientOriginalName();

        Image::make($contentImage)->resize(300, 300)->save('files/1/Gallery/Content/'.$filename);
        return ('files/1/Gallery/Content/'.$filename);

    }
}
