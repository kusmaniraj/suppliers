<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class GalleryModel extends Model
{
    protected $table='galleries';
    protected $fillable=['id','content_id','_token','created_at','images'];
}
