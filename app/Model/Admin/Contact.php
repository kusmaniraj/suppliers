<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $table='contacts';
    protected $fillable = ['id',
        'name', 'email', 'password','profileImg','gender','created_at','status','message'
    ];
    protected $hidden = [
        '_token',
    ];
}
