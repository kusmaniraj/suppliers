<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ContentsModel extends Model
{
    protected $table='contents';

    protected $fillable=['_token','id','name','slider','description','status','featuredImg','categoryId','created_at'];

    public  function category(){
        return $this->belongsTo('App\Model\Admin\Categories','categoryId');
    }
    public function galleries(){
    	return $this->hasMany('App\Model\Admin\GalleryModel','contentId');
    }
}
