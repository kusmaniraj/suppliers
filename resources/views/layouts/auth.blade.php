<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')|{{ $getSetting['website_name'] }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('admin/adminlte/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/adminlte/css/AdminLTE.min.css')}}">
    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
   
    <link rel="stylesheet" href="{{asset('admin/css/styles-admin.css')}}">
   

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">

        @yield('content')
    </div>

    <!-- Scripts -->
   <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- <script src="{{asset('admin/adminlte/js/app.min.js')}}"></script> -->
</body>
</html>
