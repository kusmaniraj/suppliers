<!DOCTYPE html>

<!-- Mirrored from themes.webinane.com/lifeline/index6.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jul 2017 16:59:15 GMT -->
<head>
    @include('web.includes.head')

</head><!-- Head -->

<body >
<div class="theme-layout">

    <div id="top-bar" class="modern">
        <div class="container">
            <ul>
                <li>
                    <i class="icon-home"></i>
                    {{$setting['owner_address']}}
                </li>
                <li>
                    <i class="icon-phone"></i>
                    {{$setting['owner_contact']}}
                </li>
                <li>
                    <i class="icon-envelope"></i>
                    {{$setting['owner_email']}}
                </li>
            </ul>
            <div class="header-social">
                <ul>
                    <li><a href="{{$setting['google_plus_url']}}" title="Google +"><i class="icon-google-plus"></i></a>
                    </li>
                    <li><a href="{{$setting['fb_url']}}" title="Facebook"><i class="icon-facebook"></i></a></li>
                    <li><a href="{{$setting['skype_url']}}" title="Skype"><i class="icon-skype"></i></a></li>

                    <li><a href="{{$setting['twitter_url']}}" title="Twitter"><i class="icon-twitter"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!--top bar-->

    <header class="header2 sticky">
        @include('web.includes.header')
    </header><!--header-->

    <div class="responsive-header">

        @include('web.includes.responsive_header')
    </div><!--Responsive header-->

    @yield('content')

</div>
<footer
    style="background-image:url({{asset('web/images/home.jpg')}}); background-repeat:repeat; background-position:0 0; background-attachment:fixed; ">
    @include('web.includes.footer')
</footer><!-- Footer -->

<div class="footer-bottom">
    @include('web.includes.footer_bottom')
</div><!-- Bottom Footer Strip -->


@include('web.includes.script')


</body>


</html>
