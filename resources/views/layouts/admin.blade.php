
<!DOCTYPE html>
<html>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" class="img-responsive img-circle" type="image/x-icon" href="{{asset('logo.png')}}" />
    <title>{{ $getSetting['website_name'] }} |  @isset($title){{$title}}@endisset</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('admin/adminlte/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/adminlte/css/AdminLTE.min.css')}}">
    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/adminlte/css/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/adminlte/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/adminlte/plugins/datatable/datatable_bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/adminlte/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/styles-admin.css')}}">
    <link href="{{asset('admin/adminlte/plugins/datatable/datatables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/adminlte/plugins/fileInput/fileinput.min.css')}}" rel="stylesheet">

    @stack('styles')




    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href=#" class="logo">
            <span class="logo-mini"><b>{{ $getSetting['website_name_short'] }}</b></span>
        <span class="logo-lg"><b>{{ $getSetting['website_name'] }}
            </b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
            <?php if(Auth::user()->gender=='Male'){
                               $defaultImg=asset('admin/images/male.jpg');
                               }else{
                                $defaultImg=asset('admin/images/female.png');
                              }?>
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{Auth::user()->profileImg ? asset('files/1/profile/'.Auth::user()->profileImg ) : $defaultImg }}" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                               <h1>{{Auth::user()->name}}</h1>


                                <img src="{{Auth::user()->profileImg ? asset('files/1/profile/'.Auth::user()->profileImg ) : $defaultImg }}" class="img-circle"
                                     alt="User Image">


                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{url('control_panel/profile')}}"
                                       class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <form action="{{ route('logout') }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-default btn-flat">Sign
                                            out</button>
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class=" {{$title=='Dashboard' ? 'active':'' }}">
                    <a href="{{url('control_panel/')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                </li>

                <li class="header">Pages</li>


                <li class="{{$title=='Menu' ? 'active':'' }}">
                    <a href="{{url('control_panel/menu')}}"><i class="fa fa-navicon"></i> <span>Menu </span>
                    </a>
                </li>

                <li class="header">Contact</li>


                <li class="{{$title=='Contact' ? 'active':'' }}">
                    <a href="{{url('control_panel/contact')}}"><i class="fa fa-envelope"></i> <span>Customer Messages </span>
                        <span class="pull-right-container">
                                    @php
                                    $countRead=App\Model\Admin\Contact::where('status','read')->count();
                                     $countUnRead=App\Model\Admin\Contact::where('status','unread')->count();
                                    @endphp


                                 <small class="label pull-right bg-green">{{$countRead}}</small>
                                <small class="label pull-right bg-red">{{$countUnRead}}</small>
                         </span>
                    </a>
                </li>







                <li class="header">Page Categories</li>


                <li class="{{$title=='Categories' ? 'active':'' }}">
                    <a href="{{route('categories.index')}}"><i class="fa fa-list-alt"></i> <span>Categories </span>
                    </a>

                <li class="{{$title=='Contents' ? 'active':'' }}">
                    <a href="{{route('contents.index')}}"><i class="fa fa-align-left"></i> <span>Contents </span>
                    </a>
                </li>
<!--                <li>-->
<!---->
<!--                    <a id="lfm" data-input="thumbnail" data-preview="holder" style="cursor: pointer;"><i class="fa fa-file"></i> <span>File Manager -->
<!--                    </a>-->
<!--                </li>-->
                <li class="header">Setting & Profile</li>
                <li class="{{$title=='Profile' ? 'active':'' }}">

                    <a href="{{url('control_panel/profile')}}"><i class="fa fa-user"></i> <span>Profile 
                    </a>
                </li>

                <li class="{{$title=='Setting' ? 'active':'' }}">
                    <a href="{{route('setting.index')}}"><i class="fa fa-gears"></i> <span>Setting </span>
                    </a>
                </li>




            </ul>
        </section>
    </aside>
    @yield('content')
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version {{$getSetting['version']}}</b>
        </div>
        <strong>Copyright ©2013 <a href="http://bit2013.com.np" target="_blank">{{ $getSetting['website_name'] }}</a>.</strong> All rights
        reserved.
    </footer>

</div>

<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin/adminlte/js/app.min.js')}}"></script>
<script src="{{asset('admin/adminlte/js/bootstrap-timepicker.min.js')}}"></script>
{{--Filemanager--}}
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('admin/adminlte/plugins/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/adminlte/plugins/datatable/datatables.bootstrap.js')}}"></script>
<script src="{{asset('admin/adminlte/plugins/fileInput/fileinput.min.js')}}"></script>
@stack('scripts')
<script>
    $('#lfm').filemanager('file');

</script>
<script>
    $("#msg-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
</script>
<script>
    $('#msgDel').click(function () {
        var ask_permission = confirm("Are you sure,you want to Delete ?");
        if(ask_permission == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    });
</script>
</body>

</html>
