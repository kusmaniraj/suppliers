@php
     use App\Model\Admin\Setting;
$setting=Setting::first();
$name=$setting['website_name'];
$logo=$setting['logo'];
@endphp
<html>
    <head>
        <title>Page Not found</title>
        <link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('errors/errors.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body style="background-image:url({{asset('admin/images/pattern5.jpg')}}); background-repeat:repeat; background-position:0 0; background-attachment:fixed; ">

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="error-template">
                        <h1>{{$name}}</h1>
                        <h1>
                            Oops!</h1>
                        <h2>
                            404 Not Found</h2>
                        <div class="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                        <div class="error-actions">
                            <a href="{{url('/')}}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                                Take Me Home </a><a href="{{url('/contact/')}}" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>