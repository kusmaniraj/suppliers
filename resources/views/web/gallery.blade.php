
@extends('layouts.web')
@section('content')
@include('web.includes.top_image')
		<section class="inner-page">
			<div class="container">
					<div class="page-title">
						<h1>सप्लायर्स  <span> &nbsp ग्यालरी </span></h1>
					</div><!-- Page Title -->
					
						
						<div class="galleries-content col-md-12">
							<div class="row">
								@isset($contents)
								@foreach($contents as $content )
									
								<div class="col-md-3">
									<div class="gallery-image">
										@if(empty($content['featuredImg']))
										<img src="{{asset('web/images/blank-image.jpg')}}" alt="" />

										@elseif(file_exists(public_path('files/1/'.$content['featuredImg'])))
										<img src="{{asset('files/1/'.$content['featuredImg'])}}" alt="" >

										@else

											<img src="{{asset('web/images/blank-image.jpg')}}" alt="" />

										@endif

										<a href="{{route('list_gallery',$content['id'])}}"><span><h3>{{$content['name']}}</h3></span></a>
										
									</div>
									
								</div>
								@endforeach

								
								<div class="pagination-area">
						            <ul class="pagination">
						                <li>{{ $contents->links() }}</li>
						                
						            </ul>

						            

						            <span>Page 1 of 8</span>
						        </div><!--Pagination-->


								@endisset
							</div>
						</div>	

					@isset($galleries)
						<div class="galleries-content col-md-9">
							<div class="row">
									
								@foreach($galleries as $image )
									
								<div class="col-md-4">
									<div class=" galleries">
										<div class="galleries-image-list">
											@if(empty($image['images']))
											<a href="{{asset('web/images/home.jpg')}}" class="html5lightbox galleries-image" title=" ">
												<img src="{{asset('web/images/home.jpg')}}" alt="" />
											</a>

											@elseif(file_exists(public_path($image['images'])))
											<a href="{{asset($image['images'])}}" class="html5lightbox galleries-image" title=" ">
												<img src="{{asset($image['images'])}}" style="height: 300px" alt="" />
											</a>

											@else
											<a href="{{asset('web/images/home.jpg')}}" class="html5lightbox galleries-image" title=" ">
												<img src="{{asset('web/images/home.jpg')}}" alt="" />
											</a>
											@endif
				                       

				                       
				                       
				                   		 </div>
									</div>
									
								</div>
								@endforeach
								<div class="pagination-area">
						            <ul class="pagination">
						                <li>{{ $galleries->links() }}</li>
						                
						            </ul>
						            
						        </div><!--Pagination-->
						       
								
								
							</div>
				
					</div>
					 @isset($list_galleries)
					<div class="sidebar col-md-3 ">
						<div class="sidebar-widget">
                    <div class="sidebar-title">
                        <h4>समाग्री <span>&nbsp ग्यालरी</span></h4>
                    </div>
                    <ul class="sidebar-list">

                        @foreach($list_galleries as $list_gallery)
                        <li><a href="{{route('list_gallery',$list_gallery['id'])}}" title="{{$list_gallery['name']}}">{{$list_gallery['name']}}</a></li>
                        @endforeach
                    </ul>
               		    </div><!-- Category List -->
              		 </div>
              		 <!-- list galleries -->

            		 @endisset
            		 <!-- galleries -->
            		 @endisset
			</div>
			<!-- end container -->
</section>
<!-- end section -->
@endsection