@extends('layouts.web')
@section('content')
@include('web.includes.top_image')

<section class="inner-page">
    <div class="container">
        <div class="page-title">
            <h1>सप्लायर्स    <span>सम्पर्क</span></h1>
        </div><!-- Page Title -->
        <div class="row">
            <div class="col-md-6">
                <div class="contact-info">
                    <h3 class="sub-head">सम्पर्क ठेगाना</h3>
                    <iframe src=" {{$setting['google_maps_iframe']}}" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                   
                     
                    <ul class="contact-details">
                        <li>
                            <span><i class="icon-home"></i>ठेगाना</span>
                            <p>{{$setting['owner_address']}}</p>
                        </li>
                        <li>
                            <span><i class="icon-phone-sign"></i>फोन न.</span>
                            <p>{{$setting['owner_contact']}}</p>
                        </li>
                        <li>
                            <span><i class="icon-envelope-alt"></i>इमेल</span>
                            <p>{{$setting['owner_email']}}</p>
                        </li>

                    </ul>
                </div>
            </div>	<!-- Contact Info -->
            <div class="col-md-6">
                <div id="message"></div>
                <div class="form">
                    <h3 class="sub-head">सूचनाबाट सम्पर्क गर्न</h3>
                    
                   @include('alertMessage')
                    <form method="post"  action="{{url('sendMail')}}" >
                        {{csrf_field()}}
                       
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                 <label for="name" accesskey="U">नाम <span>*</span></label>
                                    <input type="text" name="name" value="{{old('name')}}" class="form-control"
                                           >
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                 <label for="email" accesskey="E">इमेल <span>*</span></label>
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control"
                                           >
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                            </div>

                             <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                 <label for="message" accesskey="C">सूचना <span>*</span></label>
                                  <textarea name="message" rows="9" id="comments" rows="7" class="form-control "> {{old('message')}}</textarea>
                                  @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                             </div>
                            
                 

                        <input type="submit" class="form-button submit" id="submit" value="सूचना पठाउनु" />
                    </form>
                    <div class="g-recaptcha" data-sitekey="6Lfp2yETAAAAAJpa6Hjx8XXb5lCk8zLzFlxNOlxe"></div>

                </div>
            </div>	<!-- Message Form -->
        </div>
    </div>

    <div class="social-connect">
        <div class="container">
            <h3>सामाजिक   सन्जाल</h3>
            <ul class="social-bar">
                <li><a href="{{$setting['fb_url']}}" title="Facebook"><img src="{{asset('web/images/facebook.png')}}" alt="" /></a></li>
                <li><a href="{{$setting['google_plus_url']}}" title="Google +"><img src="{{asset('web/images/google-plus.png')}}" alt="" /></a></li>
                <li><a href="{{$setting['skype_url']}}" title="Skype"><img src="{{asset('web/images/skype.png')}}" alt="" /></a></li>
                <li><a href="{{$setting['twitter_url']}}" title="Twitter"><img src="{{asset('web/images/twitter.png')}}" alt="" /></a></li>
            </ul>
        </div>
    </div><!-- Social Media Bar -->

 
</section>
@endsection