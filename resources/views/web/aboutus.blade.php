@extends('layouts.web')
@section('content')
@include('web.includes.top_image')

<section class="inner-page">
    <div class="container">
        <div class="page-title">
            <h1> <span>हाम्रो </span> बिषयमा</h1>
        </div><!--Page Title-->
        <div class="row">
            <div class="profile-page">
                <div class="col-md-4">
                    <div class="tab-content profile-tabs-content" id="myTabContent">
                        <div id="profile-pic1" class="tab-pane fade in active">
                            @if(file_exists(public_path('files/1/profile/'.$user['profileImg'])))
                            <img src="{{asset('files/1/profile/'.$user['profileImg'])}}" alt="" draggable="false">
                            @else
                            <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                            @endif
                        </div>

                    </div>




                </div>
                <div class="col-md-8">
                    <h1><i class="icon-user"></i>{{$setting['owner_name']}}</h1>
                    <span class="designation">प्रमुख  ब्यक्ती</span>
                    <ul class="profile-info">

                        <li>
                            <span><i class="icon-envelope"></i>Email:</span>
                            <p>{{$setting['owner_email']}}</p>
                        </li>
                        <li>
                            <span><i class="icon-phone"></i>Phone:</span>
                            <p>{{$setting['owner_contact']}}</p>
                        </li>
                    </ul>
                <p>{{$setting['short_description']}}</p>

                </div>


            </div>
        </div>
    </div>


</section>
@endsection