<?php
use App\Model\Admin\Categories;

?>
@extends('layouts.web')
@push('styles')
<link rel="stylesheet" href="{{asset('web/layerslider/css/layerslider.css')}}" type="text/css">
@endpush
@section('content')
@include('alertMessage')

@isset($sliders)


<div id="layerslider-container-fw">
    <div id="layerslider" style="width: auto; height:400px; margin: 0px auto; ">
        @php
        $transitionNum=[0=>
        12,1=>35,2=>53,3=>75,4=>63];
        @endphp
        @foreach($sliders as $key=>$slider)
        <?php
        $category = Categories::select('name')->where('id', $slider['categoryId'])->first();
        ?>

        <div class="ls-slide" data-ls="transition2d:{{$transitionNum[$key]}}timeshift:-1000;">
            @if(empty($slider['featuredImg']))
            <img src="{{asset('web/images/blank-image.jpg')}}"  class="ls-bg" alt="" />
            @elseif(file_exists(public_path('files/1/'.$slider['featuredImg'])))
            <img src="{{asset('files/1/'.$slider['featuredImg'])}}"  class="ls-bg" alt="" />
            @else
            <img src="{{asset('web/images/blank-image.jpg')}}"   class="ls-bg"alt="" />
            @endif



            <h3 class=" ls-l slide3"
                style="top: 250px; left:20px;  padding:10px 50px 10px 50px; background:rgba(34, 34, 34, 0.7); border-radius:4px 0 0px 4px; border-left:5px solid #93b631; position:relative; line-height:22px; float:left;"
                data-ls="offsetxin:0;offsetyin:top;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:10;offsetxout:0;durationout:1500;">
                <a href="{{route('detail.category_type',[$slider['categoryId'],$slider['id']])}}"
                   style="font-family:roboto; font-size:24px; font-weight:600; color:#fff; cursor: pointer">{{($slider['name'])}}
                </a>
            </h3>


            <p class="ls-l slide1"
               style=" background:rgba(34, 34, 34, 0.7); padding: 10px; top: 292px; left:20px; width: 50%; font-family:roboto; font-size:15px; color:#fefefe;"
               data-ls="delayin:1000; scalein:4; durationin : 1000;">{{str_limit($slider['description'],200)}}</p>

        </div><!-- Slide1 -->
        @endforeach


    </div>
</div><!-- Layer Slider -->
@endisset

<section>
    <div class="container">
        <div class="message-box">
            <div class="message-box-title">
                <span><i class="icon-envelope-alt"></i></span>

                <p>सम्पर्क सन्देश</p>
                <i class="icon-angle-up"></i>
            </div>
            <div class="message-form">
                <h5>* &nbsp सूचना छाड्नु होला हामी संग स्पर्श हुनको लागि &nbsp *</h5>

                <form method="post" action="{{url('sendMail')}}">
                    {{csrf_field()}}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" accesskey="U">नाम <span>*</span></label>
                        <input type="text" name="name" value="{{old('name')}}" class="form-control" required
                        >
                        @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" accesskey="E">इमेल <span>*</span></label>
                        <input type="email" name="email" value="{{old('email')}}" class="form-control" required
                        >
                        @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label for="message" accesskey="C">सुचना <span>*</span></label>
                        <textarea name="message" id="comments" rows="4" class="form-control " required> {{old('message')}}</textarea>
                        @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                        @endif
                    </div>


                    <input type="submit" class="form-button submit btn btn-sm" id="submit" value="सूचना पठाउनु"/>
                </form>

            </div>
        </div><!-- Message Box -->
    </div>
</section>
<!-- contact message -->

<div class="row">
    <div class="col-md-2 col-sm-3 top-notice">
        सूचना
    </div>
    <div class="col-md-10 col-sm-9">
         <marquee class="notice-marque" direction=" left
    " onmouseover="stop(this)" onmouseout="start(this)" class="wow fadeInUp animated" data-wow-delay=".6s">

        <p style=" ">
            {{$setting['website_notice']}}
        </p>
    </marquee>
    </div>


   
</div>

<section class="block  ">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="sec-title">
                    <h2>हालको<span>&nbsp प्रस्ताव</span></h2>
                </div>

                <div class="mission-carousel">


                    @isset($list_offers)
                    <ul class="slides" style="width: 400%;">
                        @foreach($list_offers->chunk(3) as $key=>$chunk_offers)
                        <li
                            style="float: left; display: block; width: 870px;">
                            <div class="row">
                                @foreach($chunk_offers as $chunk_offer)
                                <div class="col-md-4">
                                    <div class="single-mission">
                                        <div class="mission-img">
                                            <a href="{{route('detail.category_type',[$chunk_offer['categoryId'],$chunk_offer['id']])}}"
                                               title="">
                                                @if(empty($chunk_offer['featuredImg']))
                                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                                @elseif(file_exists(public_path('files/1/'.$chunk_offer['featuredImg'])))
                                                <img src="{{asset('files/1/'.$chunk_offer['featuredImg'])}}" alt="" />
                                                @else
                                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                                @endif

                                            </a>
                                        </div>
                                        <h3>
                                            <a href="{{route('detail.category_type',[$chunk_offer['categoryId'],$chunk_offer['id']])}}"
                                               title="">{{$chunk_offer['name']}}</a></h3>

                                        <p>{{str_limit($chunk_offer['description'],250)}}</p>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </li>

                        @endforeach
                    </ul>


                    @else
                    <ul class="slides"
                        style="width: 400%;">
                        <li class="flex-active-slide" style="float: left; display: block; width: 870px;">
                            <div class="col-md-12">
                                <h3>प्रस्ताव छैन</h3>

                            </div>
                        </li>

                    </ul>


                    @endisset


                </div>
            </div>

            <div class="col-md-3">
                <div class="sec-title">
                    <h2>
                        हामीलाई <span>&nbsp जोड्नुहोस् </span></h2>
                </div>
                <iframe src="{{$setting['fb_page']}}" width="300" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
               
            </div>
        </div>

    </div>
    </div>
    <div>
</section>
<!-- NOTICES-->
@isset($list_products)
<section class="block remove-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sec-title">
                    <h2>हाम्रो<span>&nbsp सामग्री</span></h2>
                </div>
                <div class="main-blog row">

                    @foreach($list_products as $product)
                    <div class="col-md-3">
                        <div class="blog-post">
                            <h2><a title="{{$product['name']}}"
                                   href="{{route('detail.category_type',[$product['categoryId'],$product['id']])}}">{{$product['name']}}</a>
                            </h2>
                            <a title="" href="{{route('detail.category_type',[$product['categoryId'],$product['id']])}}"
                               class="blog-post-img">
                                @if(empty($product['featuredImg']))
                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                @elseif(file_exists(public_path('files/1/'.$product['featuredImg'])))
                                <img src="{{asset('files/1/'.$product['featuredImg'])}}" />
                                @else
                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                @endif
                            </a>

                            <div class="blog-post-details">

                                <div class="post-desc">
                                    <p>{{str_limit($product['description'],200)}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>


        </div>
    </div>
</section>
@endisset

<!-- Featured products -->
<section class="block remove-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12 column">
                <div class="sec-title">
                    <h2>हाम्रो<span>&nbsp सुविधा</span></h2>
                </div>
                <div class="remove-ext">
                    <div class="col-md-12">
                        @empty($list_facilitiess)
                        <div class="container">
                            <h3> हालको सुविधा छैन</h3>
                        </div>
                        @else
                        @foreach($list_facilitiess as $facility)
                        <div class="col-md-4">
                            <div class="recent-news">
                                <div class="row">
                                    <div class="col-md-5">
                                        <a class="news-img"
                                           href="{{route('detail.category_type',[$facility['categoryId'],$facility['id']])}}"
                                           title="">
                                            @if(empty($facility['featuredImg']))
                                            <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                            @elseif(file_exists(public_path('files/1/'.$facility['featuredImg'])))
                                            <img src="{{asset('files/1/'.$facility['featuredImg'])}}" alt="" />
                                            @else
                                            <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                            @endif

                                        </a>
                                    </div>
                                    <div class="col-md-7">
                                        <h4>
                                            <a href="{{route('detail.category_type',[$facility['categoryId'],$facility['id']])}}"
                                               title="">{{$facility['name']}}</a></h4>

                                        <p>{{str_limit($facility['description'],120)}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endempty

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>
<!-- Offers -->

@isset($list_testimonials)
<section class="block remove-top">
    <div class="container">

        <div class="row">
            <div class="fixed-bg testimonial-color"
                 style="background-image:url({{asset('admin/images/pattern2.png')}}); background-repeat:repeat; background-position:0 0; background-attachment:fixed; ">

            </div>
            <div class="testimonial overlap">
                <div class="slideshow">
                    <ul class="slides">
                        @foreach($list_testimonials as $list_testimonial)
                        <li>
                            <div class="carusal-image-thumb">
                                @if(empty($list_testimonial['featuredImg']))
                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                @elseif(file_exists(public_path('files/1/thumbs/'.$list_testimonial['featuredImg'])))
                                <img src="{{asset('files/1/thumbs/'.$list_testimonial['featuredImg'])}}" alt="thumb1"/>
                                @else
                                <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                                @endif

                                <strong>{{$list_testimonial['name']}}</strong>
                            </div>
                            <p>{{ str_limit($list_testimonial['description'],200)}}</p>
                        </li><!-- Messgae1 -->
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>

    </div>
</section>
@endisset
<!-- Testimonials -->


@endsection
@push('scripts')

<script src="{{asset('web/js/testimonials.js')}}"></script>

<!-- Scripts For Layer Slider  -->
<script src="{{asset('web/layerslider/js/greensock.js')}}" type="text/javascript"></script>
<!-- LayerSlider script files -->
<script src="{{asset('web/layerslider/js/layerslider.transitions.js')}}" type="text/javascript"></script>
<script src="{{asset('web/layerslider/js/layerslider.kreaturamedia.jquery.js')}}" type="text/javascript"></script>
<script defer src="{{asset('web/js/jquery.flexslider.js')}}"></script>

<script>
    $(document).ready(function () {
        jQuery("#layerslider").layerSlider({
            responsive: true,
            responsiveUnder: 1280,
            layersContainer: 1170,
            skin: 'fullwidth',
            hoverPrevNext: true,
            skinsPath: 'web/layerslider/skin/'
        });


    });
</script>
<script>
    $(window).load(function () {
        $('.causes-carousel').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            pausePlay: false,
            mousewheel: false,
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
        $('.mission-carousel').flexslider({
            animation: "slide",
            animationLoop: false,
            slideShow: false,
            controlNav: false,
            maxItems: 1,
            pausePlay: false,
            mousewheel: false,
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });


        $('.ongoing-projects').flexslider({
            animation: "slide",
            animationLoop: false,
            slideShow: false,
            controlNav: false,
            maxItems: 1,
            pausePlay: false,
            mousewheel: false,
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });


        $('.slideshow').flexslider({
            animation: "fade",
            animationLoop: false,
            slideShow: false,
            controlNav: true,
            maxItems: 1,
            pausePlay: false,
            mousewheel: false,
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });

    });
</script>
@endpush