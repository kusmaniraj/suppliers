@extends('layouts.web')
@section('content')
@include('web.includes.top_image')

<section class="inner-page">
    <div class="page-title">
        <h1> <span>सप्लायर्स</span>सामाग्रीहरु</h1>
    </div><!--Page Title-->
    <div class="container">

        @empty($list_categories)
        <h3>No lists {{$category_name}}</h3>
        @else
        <div class="row">
            @foreach($list_categories as $list_category)
            <div class="col-md-3">
                <div class="story">
                    <div class="story-img">

                        @if(empty($list_category['featuredImg']))
                        <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                        @elseif(file_exists(public_path('files/1/'.$list_category['featuredImg'])))
                        <img src="{{asset('files/1/'.$list_category['featuredImg'])}}"  />
                        @else
                        <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                        @endif


                        <h5>{{$list_category['name']}}</h5>
                        <a href="{{route('detail.category_type',[$list_category['categoryId'],$list_category['id']])}}" title="{{$list_category['name']}}"><span></span></a>
                    </div>
                    <p>{{str_limit($list_category['description'],100)}}</p>
                    <!-- <h6><i>$</i> 85920<span>Money Spent</span></h6>
                    <span><i class="icon-map-marker"></i>In SouthAfrica</span> -->
                </div><!--Story-->
            </div>
            @endforeach
        </div>
       
        <div class="pagination-area">
            <ul class="pagination">
                <li>{{ $list_categories->links() }}</li>
                
            </ul>

            

            <span>पृष्ठ  १ को ४</span>
        </div><!--Pagination-->
         @endempty
    </div>



</section>
@endsection