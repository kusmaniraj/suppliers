@extends('layouts.web')
@section('content')
@include('web.includes.top_image')
<?php
    if($detail_category){
        $category_name=ucfirst($detail_category['name']);
        if(ucfirst($detail_category['name'])=='Events'){
            $category_name='कार्यक्रम';
        }elseif(ucfirst($detail_category['name'])=='Offers'){

            $category_name='प्रस्ताव';

        }elseif(ucfirst($detail_category['name'])=='Products'){
            $category_name='सामाग्री';
        }
    }else{
        $category_name='';
    }
?>

<section class="inner-page">
     <div class="page-title">
            <h1>{{$category_name}}<span> &nbsp {{$detail_content['name']}}</span></h1>
    </div><!--Page Title-->
    <div class="container">
        <div class="row">
            <div class="left-content col-md-9">
                <div class="post">
                    @if(empty($detail_content['featuredImg']))
                    <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                    @elseif(file_exists(public_path('files/1/'.$detail_content['featuredImg'])))
                    <img src="{{asset('files/1/'.$detail_content['featuredImg'])}}"   />
                    @else
                    <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                    @endif




                    <!-- <span class="content">In <a href="#" title="">Home</a>, <a href="#" title="">Blog</a>, <a href="#" title="">Charity</a></span> --><!-- Categories -->
                    <h1>{{$detail_content['name']}}</h1>

                    <div class="post-desc">
                        <p>{{$detail_content['description']}} </p>
                    </div>

                    <div class="post-image-list">
                        @foreach($content_images as $content_image)
                       
                            @if(empty($content_image['images']))
                             <a href="{{asset('web/images/blank-image.jpg')}}" class="html5lightbox post-image" title="{{$content_image['name']}} ">
                            <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                            @elseif(file_exists(public_path($content_image['images'])))
                             <a href="{{asset($content_image['images'])}}" class="html5lightbox post-image" title="{{$content_image['name']}} ">
                            <img src="{{asset($content_image['images'])}}" style="height: 300px" alt="" />
                            @else
                             <a href="{{asset('web/images/blank-image.jpg')}}" class="html5lightbox post-image" title="{{$content_image['name']}} ">
                            <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                            @endif

                        </a>
                        @endforeach
                       
                    </div><!-- Post Images -->

                </div>




            </div>

            <div class="sidebar col-md-3 pull-right">
               
                <div class="sidebar-widget">
                    <div class="sidebar-title">
                        <h4>सप्लायर्स <span>&nbsp {{$category_name}}</span></h4>
                    </div>
                    <ul class="sidebar-list">
                        @foreach($list_categories as $list_content)
                        <li><a href="{{route('detail.category_type',[$list_content['categoryId'],$list_content['id']])}}" title="{{$list_content['name']}}">{{$list_content['name']}}</a></li>
                        @endforeach
                    </ul>
                </div><!-- Category List -->
               

               
                

            </div>

        </div>
    </div>


</section>
@endsection