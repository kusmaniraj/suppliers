
@php
use App\Http\Controllers\BuildTree;
$tree=new BuildTree;


@endphp
    <div class="container">
        <p>Copyright © {{date('Y')}} {{$setting['website_name']}}. <span>All rights reserved.</span> </p>
        <style type="text/css">
            #menuFooter-navigation{
                text-transform: uppercase;
            }
        </style>
        {!! $tree->buildFooterMenu($menus)!!}


    </div>
