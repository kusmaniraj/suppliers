<div class="top-image">

	@if(empty($menuImg['featuredImg']))
	<img src="{{asset('web/images/home.jpg')}}" alt="" />
	@elseif(file_exists(public_path('files/1/'.$menuImg['featuredImg'])))
	<img src="{{asset('files/1/'.$menuImg['featuredImg'])}}" style="height: 300px" alt="" />
	@else
	<img src="{{asset('web/images/home.jpg')}}" alt="" />
	@endif

</div><!-- Page Top Image -->