<div class="container">
    <div class="logo">
        <a href="{{url('/')}}" title=""><h1>{{$setting['website_name']}}</h1></a>
    </div><!-- Logo -->


    <nav class="menu">

        @php
        use App\Http\Controllers\BuildTree;
        $tree=new BuildTree;


        @endphp
        {!!$tree->buildMenu($menus)!!}

    </nav><!-- Menu -->



</div>