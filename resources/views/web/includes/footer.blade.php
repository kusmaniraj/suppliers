<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="footer-widget-title">
                <h4><strong><span>ग्राहकको</span></strong>&nbsp  समिक्षा </h4>
            </div>
            <div class="footer_carousel">
                <ul class="slides">
                    @isset($list_PeopleReviews)
                    @foreach($list_PeopleReviews as $list_PeopleReview)
                    <li>
                        <div class="review">
                            <p>{{str_limit($list_PeopleReview['description'],200)}} </p>
                        </div>
                        <div class="from">
                            <h6>{{$list_PeopleReview['name']}}</h6>
                            <!-- <span>CE0, Australia</span> -->
                        </div>
                    </li>
                    @endforeach
                    @endisset

                </ul>
            </div>
        </div><!-- Reviews Widget -->
        <div class="col-md-3">
            <div class="footer-widget-title">
                <h4><strong><span>फ्लिकर </span></strong>&nbsp  फोटो</h4>
            </div>
            <div class="flickr-images">
                @isset($sliders)
                @foreach($sliders as $slider)

                <a href="{{route('detail.category_type',[$slider['categoryId'],$slider['id']])}}" title="{{$slider['name']}}">
                    @if(empty($slider['featuredImg']))
                    <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                    @elseif(file_exists(public_path('files/1/thumbs/'.$slider['featuredImg'])))
                    <img src="{{asset('files/1/thumbs/'.$slider['featuredImg'])}}" alt="" />
                    @else
                    <img src="{{asset('web/images/blank-image.jpg')}}" alt="" />
                    @endif

                </a>
                @endforeach
                @endisset

            </div>
        </div><!-- Flickr Widget -->
        <div class="col-md-3">
            <div class="footer-widget-title">
                <h4><strong><span>सप्लायर्स </span></strong> &nbsp  सम्पर्क</h4>
            </div>
            <ul class="contact-details">
                <li>
                    <span><i class="icon-home"></i>ठेगाना</span>
                    <p>{{$setting['owner_address']}}</p>
                </li>
                <li>
                    <span><i class="icon-phone-sign"></i>फोन न.</span>
                    <p>{{$setting['owner_contact']}}</p>
                </li>
                <li>
                    <span><i class="icon-envelope-alt"></i>इमेल</span>
                    <p>{{$setting['owner_email']}}</p>
                </li>

            </ul>
        </div><!-- Contact Us Widget -->
        <div class="col-md-3">
            <div class="footer-widget-title">
                <h4><strong><span>सामाजिक </span></strong> &nbsp सन्जाल</h4>
            </div>

            <ul class="social-bar">

                <li><a href="{{$setting['fb_url']}}" title="Facebook"><img src="{{asset('web/images/facebook.jpg')}}" alt="" /></a></li>
                <li><a href="{{$setting['google_plus_url']}}" title="Google +"><img src="{{asset('web/images/gplus.jpg')}}" alt="" /></a></li>
                <li><a href="{{$setting['skype_url']}}" title="Skype"><img src="{{asset('web/images/skype.png')}}" alt="" /></a></li>
                <li><a href="{{$setting['twitter_url']}}" title="Twitter"><img src="{{asset('web/images/twitter.png')}}" alt="" /></a></li>
            </ul>

        </div>	<!-- Contact Link-->
    </div>
</div>