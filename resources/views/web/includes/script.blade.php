<!-- Scripts -->
<script src="{{asset('web/js/jquery-2.2.2.js')}}" type="text/javascript"></script>
<script src="{{asset('web/js/perfect-scrollbar.jquery.js')}}" type="text/javascript"></script>
<script src="{{asset('web/js/perfect-scrollbar.js')}}" type="text/javascript"></script>

 <script src="{{asset('web/js/jquery.carouFredSel-6.2.1-packed.js')}}" type="text/javascript"></script>
<script src="{{asset('web/js/script.js')}}"></script>
<script src="{{asset('web/js/bootstrap.js')}}"></script>
<script src="{{asset('web/js/html5lightbox.js')}}"></script>
<script defer src="{{asset('web/js/jquery.flexslider.js')}}"></script>
<script defer src="{{asset('web/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('web/js/styleswitcher.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/jquery.jigowatt.js')}}"></script>

<script type="text/javascript">
    $(window).load(function(){
        $('.footer_carousel').flexslider({
            animation: "slide",
            animationLoop: false,
            slideShow:false,
            controlNav: true,
            maxItems: 1,
            pausePlay: false,
            mousewheel:false,
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
@stack('scripts')