
@php
use App\Http\Controllers\BuildTree;
$tree=new BuildTree;


@endphp
<div class="responsive-topbar">
    <div class="responsive-topbar-info">
        <ul>
            <li><i class="fa fa-home"></i> {{$setting['owner_address']}}</li>
            <li><i class="fa fa-phone"></i> {{$setting['owner_contact']}}</li>
            <li><i class="fa fa-envelope"></i>{{$setting['owner_email']}}</li>
        </ul>
        <div class="container">
            <div class="responsive-socialbtns">
                <ul>

                    <li><a href="{{$setting['fb_url']}}" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="{{$setting['google_plus_url']}}" title="Google +"><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li><a href="{{$setting['twitter_url']}}" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="{{$setting['skype_url']}}" title="Skype"><i class="fa fa-skype"></i></a></li>


                </ul>
            </div>
        </div>
    </div>
</div>
<div class="responsive-logomenu">
    <div class="container responsive-logo ">
        <a href="{{url('/')}}" title=""><h1>{{$setting['website_name']}}</h1></a>
        <span class="menu-btn "><i class="fa fa-th-list"></i></span>
    </div>
</div>
<div class="responsive-menu">
    <span class="close-btn"><i class="fa fa-close"></i></span>

    {!! $tree->buildMenu($menus, true) !!}

    <!-- <li class="has-dropdown"><a href="#" title="">Portfolio</a>
        <ul>
            <li><a href="portfolio-2-column.html" title="">2 Column Wide</a></li>
            <li><a href="portfolio-3-column.html" title="">3 Column Wide</a></li>
            <li><a href="portfolio-4-column.html" title="">4 Column Wide</a></li>
        </ul>
    </li> -->


</div>