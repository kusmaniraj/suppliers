<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{$setting['website_name']}} | @isset($title){{$title}}@endisset</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link rel="shortcut icon" class="img-responsive img-circle" type="image/x-icon" href="{{asset('logo.png')}}" />
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,900italic,700italic,900,700,500italic,500,400italic,300italic,300,100italic,100|Open+Sans:400,300,400italic,300italic,600,600italic,700italic,700,800|Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700' rel='stylesheet' type='text/css'>


<!-- Styles -->
<link href="{{asset('web/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('web/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('web/css/icons.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('web/css/style.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('web/css/responsive.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('web/css/contact.css')}}" rel="stylesheet" type="text/css" /> <!-- AJAX Contact Form Stylesheet -->
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="{{asset('web/css/ie.css')}}" />
<script type="text/javascript" language="javascript" src="{{asset('web/js/html5shiv.js')}}"></script>
<![endif]-->
@stack('styles')

