@extends('layouts.admin')


@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <a href="{{route('contents.create')}}" class="btn btn-primary pull-right"><i
                    class="fa fa-plus"></i>Add
            Web Content</a>
        <h1>
            Page Content
        </h1>
    </section>
    @include('alertMessage')


    <section class="content">
        <table class="table table-bordered" id="content_table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Featured Image</th>
                <th>Descriptions</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th >Action</th>

            </tr>
            </thead>
            <tbody>
            @if(empty($listContents))
                <h1>No Lists Of Content Data</h1>
            @else

                @foreach($listContents as $content )
                    <?php  $category=App\Model\Admin\ContentsModel::find($content['id'])?>
                <tr>
                    <td>{{$content['name']}}</td>
                    <td>{{$category->category()->first()->name}}</td>

                    <td><img src="{{asset('files/1/thumbs/'.$content['featuredImg'])}}" height="50px" width="50px"></td>
                    <td>{{str_limit($content['description'],100)}}</td>
                    <td>{{$content['created_at']}}</td>
                    <td>{{$content['updated_at']}}</td>
                    <td>
                        <a href="{{route('gallery.show',$content['id'])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Gallery"><i class="glyphicon glyphicon-picture"></i> </a>
                        <a href="{{route('contents.edit',$content['id'])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>

                        <form action="{{route('contents.destroy',$content['id'])}}" method="post">
                            <input type="hidden" value="delete" name="_method">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-danger delBtn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-remove"></i></button></form>

                    </td>
                </tr>

                @endforeach
            @endif

            </tbody>
        </table>
    </section>

</div>

@endsection
@push('scripts')
<script>

    $('#content_table').DataTable({
        processing:true,


    });
    $('.delBtn').on('click',function(){
        if(confirm('Are you sure want to delete ?')==false){
            return false;
        }
    })

</script>
@endpush
