@extends('layouts.admin')


@section('content')
    <?php
    $id = "";
    $name = old('name');
    $featuredImg = "";
    $description =  old('description');
    $categoryId = old('categoryId');
    $slider="no";

    $is_deletable = "yes";
    $status = 'active';
    $state = 'Add';
    $routeAction=route('contents.store');
    $method = "";
    if (isset($contents)) {
        $id = $contents['id'];
        $name = $contents['name'];
        $featuredImg = $contents['featuredImg'];
        $description = $contents['description'];
        $categoryId = $contents['categoryId'];
        $is_deletable = $contents['is_deletable'];
        $slider = $contents['slider'];
        $status = $contents['status'];
        $state = 'Edit';
        $routeAction=route('contents.update',$id);
        $method = method_field('PUT');




    }


    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <a href="{{route('contents.index')}}">Contents</a><span>|</span><?= $state ?> Web Content
            </h1>
        </section>

        <section class="content">
             @include('alertMessage')
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">



                            <form action="{{$routeAction}}" method="post"  enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{$method}}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Content Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $name ?>">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="form-group{{ $errors->has('categoryId') ? ' has-error' : '' }}">
                                    <label for="categoryId">Category</label>
                                    <select name="categoryId" id="categoryId" class="chosen-select form-control"
                                            data-placeholder="Select Category" >
                                        <option  value="" disabled selected>Select Categories</option>
                                        @foreach($categories as $category)
                                            <option
                                                    value="{{$category['id'] }}" {{($category['id'] == $categoryId) ? "selected" : "" }}  >{{$category['name']}}</option>
                                            @endforeach


                                    </select>
                                    @if ($errors->has('categoryId'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('categoryId') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <input   class="form-control hidden " type="text" value="{{$featuredImg}}" name="oldFeaturedImg">
                                    <input type="file" name="featuredImg" class="hidden">
                                    <div class="col-md-4">
                                          <button type="button" class="btn btn-primary" id="featuredImgBtn">Select Featured Image</button>
                                    </div>
                                    <div  class="col-md-4">
                                        <img src="{{asset('files/1/thumbs/'.$featuredImg) }}" id="imgPreview" height="200px" width="200px" class="img-thumbnail hidden">
                                    </div>
                                  
                                    

                                </div>

                                

                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description">Description</label>
                                    <textarea id="description_textarea" name="description" id="description"
                                             ><?php echo $description ?></textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                <div class="form-group">
                                    <label for="">Deletable</label><br>

                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="is_deletable"
                                                   value="yes" <?php echo ($is_deletable == "yes") ? 'checked' : ''; ?>> Yes
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="is_deletable"
                                                   value="no" <?php echo ($is_deletable == "no") ? 'checked' : ''; ?>>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <div class=" form-group{{ Session::get('errorSlider') ? ' has-error' : '' }}">
                                    <label for="">Slider</label><br>

                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="slider"
                                                   value="yes" <?php echo ($slider == "yes") ? 'checked' : ''; ?>> Yes
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="slider"
                                                   value="no" <?php echo ($slider == "no") ? 'checked' : ''; ?>>
                                            No
                                        </label>
                                    </div>
                                     @if (Session::get('errorSlider'))
                                        <span class="help-block">
                                        <strong>{{Session::get('errorSlider')}}</strong>
                                        </span>
                                    @endif
                                </div>


                                <div class="form-group">
                                    <label for="">Status</label><br>

                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="status"
                                                   value="active" {{($status == "active") ? 'checked' : ''}} > Active
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="status"
                                                   value="inactive" {{ ($status == "inactive") ? 'checked' : ''}}>
                                            Inactive
                                        </label>
                                    </div>
                                </div>

                                <input type="hidden" id="id" name="id" value="{{$id }}">

                                <div class="form-group">
                                    <button class="btn btn-primary mr15" type="submit"
                                            id="submitFormBtn">{{$state}} </button>
                                    <button class="btn btn-danger" type="button" id="cancelBtn"
                                            onclick="window.history.back()">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>


@endsection


@push('scripts')


<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>
    var aspectRatioInput =16/9;
</script>
<script>
    $(function() {
    $('#description_textarea').ckeditor({
        toolbar: 'Full',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
});
</script>

<script>
$('#featuredImgBtn').on('click',function(){
    $('input[name="featuredImg"]').click();
})

$('input[name="featuredImg"]').on('change',function() {
 


  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imgPreview').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
  }
   $('#imgPreview').removeClass('hidden');

});


    var img = "{{$featuredImg}}";
    if (img != "") {
        $('#imgPreview').removeClass('hidden');
    }
</script>

@endpush