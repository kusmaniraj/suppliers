@extends('layouts.admin')



@section('content')
    <?php


    $id = "";

    $name = "";
    $status = "active";
    $featuredImg= "";
    $state = "Add";
    $routeAction=route('categories.store');
    $method="";
    if (isset($category_detail)) {
        $id = $category_detail['id'];

        $name = $category_detail['name'];
         $featuredImg = $category_detail['featuredImg'];
        $status = $category_detail['status'];
        $state = "Edit";
        $routeAction=route('categories.update',$id);
        $method = method_field('PUT');
    }
    ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Page Categories
            </h1>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                     @include('alertMessage')

                    <div class="row">
                        <div class="col-sm-6">
                            <button class="btn btn-primary btn-sm" onclick="jstree.jstree('open_all')">Expand All</button>
                            <button class="btn btn-primary btn-sm" onclick="jstree.jstree('close_all')">Close All</button>
                            <button class="btn btn-success btn-sm" id="saveChangesBtn">Save Changes</button>
                            <a href="{{url('control_panel/categories')}}" class="btn btn-danger btn-sm"> <i
                                    class="fa fa-refresh"></i></a>
                            <hr>

                            <div class="form-group">
                                <input type="text" id="searchTree" class="form-control" placeholder="Search...">
                            </div>

                            <div id="tree"></div>
                        </div>

                        <div class="col-sm-6">
                            <h4><?= $state ?> Page Category</h4>
                            <hr>


                            <form action="<?= $routeAction ?>" method="post" enctype="multipart/form-data">

                                {{csrf_field()}}
                                {{$method}}
                                <meta name="_token" content="{!! csrf_token() !!}"/>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input type="text" name="name" value="<?= $name ?>" class="form-control"
                                           placeholder="Category Name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="input-group">
                                <div class="form-group row">
                                    <input   class="form-control hidden " type="text" value="{{$featuredImg}}" name="oldFeaturedImg">
                                    <input type="file" name="featuredImg" class="hidden">
                                    <div class="col-md-6">
                                          <button type="button" class="btn btn-primary" id="featuredImgBtn">Select Featured Image</button>
                                    </div>
                                    <div  class="col-md-4">
                                        <img src="{{asset('files/1/thumbs/'.$featuredImg) }}" id="imgPreview" height="200px" width="200px" class="img-thumbnail hidden">
                                    </div>
                                  
                                    

                                </div>
                                </div>






                                <div class="form-group">
                                    <label for="">Status</label><br>

                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="status"
                                                   value="active" <?= ($status == "active") ? 'checked' : ''; ?>>
                                            Active
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="status"
                                                   value="inactive" <?= ($status == "inactive") ? 'checked' : ''; ?>>
                                            Inactive
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary"><?= $state ?> Category</button>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>
            </div>

        </section>
        <section>
            <div class="container">
                <h3 style="color:#3c8dbc">*Note:List Category name</h3>
                <ul >
                    <li>Products</li>
                    <li>Offers</li>
                    <li>Testimonials</li>
                    <li>Gallery</li>
                    <li>Events</li>
                    <li>People Reviews</li>


                </ul>
            </div>
        </section>
    </div>
    <script>
    var aspectRatioInput = 16/ 4;
    </script>
        @include('cropper')
    //jsTree
    @endsection
   
    @push('scripts')
     <link rel="stylesheet" href="{{asset('bower_components/jstree/dist/themes/default/style.min.css')}}"/>
    <script src="{{ asset('bower_components/jstree/dist/jstree.min.js')}}"></script>

    <script>
        function customMenu(node) {
            var items = {

                editItem: {
                    label: "Edit",
                    action: function () {
                        window.location.href = "{{url('control_panel/categories')}}" + "/" + node.id + "/edit";
                    }
                },
                deleteItem: {
                    label: "Delete",
                    action: function () {
                        if (node.children.length) {
                            alert('Can not delete the category which have child categories.');
                            return;
                        }
                        $.ajax({
                            url:"{{url('control_panel/categories/')}}" + "/" + node.id ,
                            type:'DELETE',
                            data:{id:node.id,_token:'{{csrf_token()}}'},
                            success:function () {
                                window.location.href = "{{url('control_panel/categories/')}}"  ;
                            },
                            error:function () {
                                alert('error');

                            }

                        });

                    }
                }
            };
            return items;
        }

        var jstree = $('#tree');
        $(function () {

            jstree.jstree({
                'plugins': ['contextmenu', 'search', 'dnd'],
                'core': {
                    'data': <?php print_r(json_encode($categories)) ?>,
                    'themes': {
                        'icons': true
                    },
                    check_callback: true
                },
                'contextmenu': {
                    'items': customMenu
                },
                "search": {
                    "case_insensitive": true,
                    "show_only_matches": true
                },

            }).on('loaded.jstree', function () {
                jstree.jstree('open_all');
            });
        });


        $("#searchTree").keyup(function () {
            var searchString = $(this).val();
            jstree.jstree('search', searchString);
        });

        var treeExport = [];
        var debunkTree = function (categories, parentID) {

            $.each(categories, function (index, data) {
                if (data.children.length) {
                    debunkTree(data.children, data.id);
                }

                treeExport.push({
                    id: data.id,
                    name: data.text,
                    parent_id: parentID,





                });
            });
        };

        $('#saveChangesBtn').on('click', function (e) {
            var categories = jstree.jstree(true).get_json('#', {flat: false});
            treeExport = [];
            debunkTree(categories, 0);
//        console.log(treeExport);

            var params={treeExport:treeExport,_token:'{{csrf_token()}}',}
            $.ajax({
                url: "{{route('categories.saveCategories')}}",
                type: 'post',
                cache: false,
                data: params,
                success: function(response) {
//                alert(response);
                    window.location.href = "{{url('control_panel/categories/')}}"  ;
                },
                error: function() {
                    alert('error');
                }
            });
        })


    </script>
 <script>
$('#featuredImgBtn').on('click',function(){
    $('input[name="featuredImg"]').click();
})

$('input[name="featuredImg"]').on('change',function() {
 


  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imgPreview').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
  }
   $('#imgPreview').removeClass('hidden');

});


    var img = "{{$featuredImg}}";
    if (img != "") {
        $('#imgPreview').removeClass('hidden');
    }
</script>
@endpush





