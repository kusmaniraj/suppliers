@extends('layouts.auth')
@section('title','Register')



@section('content')
	<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form method="POST" action="{{ route('register') }}">
    	{{ csrf_field() }}

      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      							  @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
      </div>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}  has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        						@if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
      </div>
      <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label  class="col-md-12 control-label">Gender</label>
                                
                

                     <label class="radio-inline"><input type="radio" value="Male" 
                                                                       name="gender">Male</label>
                     <label class="radio-inline"><input type="radio" value="Female"  name="gender">Female</label>
                                     @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif


                                
                                
      </div>


      <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback" >
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        					 @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class=" col-md-offset-6 col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>

    <a href="{{ route('login') }}" class="text-center">I already have a created Account</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection