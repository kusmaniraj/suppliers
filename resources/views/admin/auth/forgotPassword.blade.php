@extends('layouts.auth')
@section('title','Forgot Password')



@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ $getSetting['website_name'] }}"><b>{{ $getSetting['website_name'] }}</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter Email  to retrieve your Password</p>
        	 @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

        

        <form method="POST" action="{{ route('password.email') }}" novalidate> {{ csrf_field() }}
             {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
               <input  type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail Address" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
            </div>
            


            <div class="row">
                <div class="col-xs-8">
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
                </div>
            </div>
        </form>
             

            <a href="{{ route('login') }}">Back to Login</a><br>
           

  

    </div>
</div>
@endsection