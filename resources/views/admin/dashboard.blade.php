<?php if(Auth::user()->gender=='Male'){
    $defaultImg=asset('admin/images/male.jpg');
}else{
    $defaultImg=asset('admin/images/female.png');
}?>
@extends('layouts.admin')



@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{Auth::user()->name}}
           
            	
            
        </h1>
    </section>

    <section class="content">
        <div class="row">




                <div class="col-md-10 col-sm-12 col-xs-12 ml70">
                    <div class="info-box">
                    <span class="info-box-icon line0"><img src="{{Auth::user()->profileImg ? asset('files/1/profile/'.Auth::user()->profileImg ) : $defaultImg }}" class=""
                                                           alt="User Image"></span>

                        <div class="info-box-content">
                            <strong class="info-box-text"></strong>
                            <span class="info-box-text"><a
                                    href="{{url('control_panel/profile')}}">Profile</a></span>
                        <span class="info-box-text"> <form action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-default btn-flat">Sign
                                    out</button>
                            </form></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
