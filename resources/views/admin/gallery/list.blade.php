@extends('layouts.admin')
@section('content')

    <div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
        <section class="content-header">
            <button type="button" class="btn btn-info pull-right" data-toggle="collapse" data-target="#gallery"><i
                        class="fa fa-plus"></i> Add Gallery
            </button>
            <h1>
               <a href="{{route('contents.index')}}">Contents</a><span>|Gallery( {{$contentDetails['name']}})
            </h1>
        </section>

        <section class="content">
           @include('alertMessage')
                @if ($errors->has('images'))
                    <div id="msg-alert" class="alert alert-danger">
                        <strong>{{ $errors->first('images') }}</strong>
                    </div>
                @endif

            <div class="row">


                <div class="col-md-12" >
                    <div class="box">
                        <div class="box-body">
                            
                            <form id="gallery" class="collapse collapse-class" action="{{route('gallery.store')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}


                                <input type="hidden" name="contentId" value="{{$contentDetails['id']}}">
                                <div class="form-group">
                                    <label class="control-label">Select File</label>
                                    <input id="input-2" name="images[]" type="file" class="file" multiple
                                           data-show-upload="false" data-show-caption="true">
                                </div>


                                <div class="form-group">
                                    <button class="btn btn-primary mr15" type="submit"
                                            id="submitFormBtn">Upload
                                    </button>
                                    <button class="btn btn-danger" type="button" id="cancelBtn"
                                            onclick="window.history.back()">Cancel
                                    </button>
                                </div>
                            </form>
                            <div class="row">
                                @isset($images)
                                   
                                    @foreach($images as $image)
                                        <div class="col-md-3">
                                            <img width="200" height="200" src="{{asset($image['images'])}}"/>

                                            <form action="{{route('gallery.destroy',$image['id'])}}"
                                                  method="post">
                                                <input type="hidden" value="delete" name="_method">
                                                <input type="hidden" value="{{$image['contentId']}}"
                                                       name="contentId">
                                                <input type="hidden" value="{{csrf_token()}}" name="_token">
                                                <button type="submit" class="label label-danger ">Remove</button>
                                            </form>
                                        </div>






                                    @endforeach
                                @endisset
                            </div>
                            <div class="pagination-area">
                                <ul class="pagination">
                                    <li>{{ $images->links() }}</li>
                                    
                                </ul>

                                

                                <span>Page 1 of 4</span>
                            </div><!--Pagination-->
                        </div>
                    </div>
                </div>

            </div>


        </section>

    </div>
@endsection