@extends('layouts.admin')


@section('content')
<div class="content-wrapper">

    <section class="content-header">


        <h1>
          Customers Message
        </h1>
    </section>
    @include('alertMessage')


    <section class="content">
        <table class="table table-bordered" id="content_table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
               
                <th>Messages</th>
                <th>Status</th>
               
                <th >Action</th>

            </tr>
            </thead>
            <tbody>
            @isset($customer_messages)
                

                @foreach($customer_messages as $message )
                   
                <tr>
                    <td>{{$message['name']}}</td>
                    <td> {{$message['email']}}   </td>

                   
                    <td>{{str_limit($message['message'],100)}}</td>
                    <td> <form action="{{route('contact.update',$message['id'])}}" method="post">

                            <input type="hidden" value="put" name="_method">
                            {{csrf_field()}}
                            @if($message['status']=='unread' )
                            <input type="hidden" value="read" name="status">
                            <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Click For Change as Read">Unread</button>
                            @else
                            <input type="hidden" value="unread" name="status">
                            <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Click For Change as Read">Read</button>
                            @endif
                        </form>


                    </td>
                   
                    <td>
                        <a  id="" c_id="{{$message['id']}}" class=" modal_msg btn btn-sm btn-info" title="Gallery"><i class="glyphicon glyphicon-eye-open"></i> </a>
                       

                        <form action="{{route('contact.destroy',$message['id'])}}" method="post">
                            <input type="hidden" value="delete" name="_method">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-remove"></i></button></form>

                    </td>
                </tr>

                @endforeach
            @endisset

            </tbody>
        </table>
         <div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Customer Message</h4>
                                </div>
                                <div class="modal-body">
                                    <p id="msg"></p>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                   
                                </div>
                            </div>
                        </div>
                    </div>
    </section>

</div>

@endsection
@push('scripts')
<script type="text/javascript">
                    $(document).ready(function(){
                        
                        $('.modal_msg').click(function(){
                     
                                 var id=$(this).attr("c_id");
                               
                    
                    var url='{{url('control_panel/contact')}}'+'/'+id;
                    // alert(url);

                    

                  $.ajax({
                                url: url,
                                type: 'get',
                                dataType: 'json',
                                     success:function($data){
                                             $('#myModalLabel').html($data['name']);
                                              $('#myModalLabel').append('<h5>('+$data['email']+')</h5>');
                                             $('#msg').html($data['message']);
                                           $('#msgModal').modal('show');
                                    },
                                error:function(data){
                                   alert('error');
                                }
              });
    });
                    });
                  
          </script>
<script>

    $('#content_table').DataTable({
        processing:true,


    });

</script>
@endpush
