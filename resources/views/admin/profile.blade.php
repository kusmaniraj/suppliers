@extends('layouts.admin')
@section('content')
<?php

$user=Auth::user();
if($user){
$id=$user['id'];
$name=$user['name'];
$email=$user['email'];
$gender=$user['gender'];
$profileImg=$user['profileImg'];
}else{

}



    	 

?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Profile
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 ">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Basic Information</h3>
					</div>	
					<div class="box-body">
					  @include('alertMessage')
						
						<form action="{{route('update.profile')}}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="user_name">Name</label>
								<input type="text" class="form-control" id="user_name" name="name"
									   value="{{$name}}">
							</div>
							 @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif

							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email">Email</label>
								<input type="email" class="form-control" id="email" name="email" value="{{$email}}">
							</div>
							 @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

							<div class="input-group">
								 <?php if(Auth::user()->gender=='Male'){
                               $defaultImg=asset('admin/images/male.jpg');
                               }else{
                                $defaultImg=asset('admin/images/female.png');
                              }?>

                              <div class="form-group row">
                                   <input type="hidden" name="old_profileImg" value="{{$profileImg ? $profileImg : $defaultImg }}">
                                    <input type="file" name="profileImg" class="hidden">
                                    <div class="col-md-6">
                                          <button type="button" class="btn btn-primary" id="profileImgBtn">Select Profile Image</button>
                                    </div>
                                    <div  class="col-md-4">
                                        <img src="{{$profileImg ? asset('files/1/profile/'.$profileImg ) : $defaultImg }}" id="imgPreview" height="200px" width="200px" class="img-thumbnail ">
                                    </div>
                                  
                                    

                                </div>
                               
                                     
                                    
                                 
                                 
                               
                                 	
                                  
                                </div>

                                
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                              		  <label for="roles">Gender</label>


                                <div class="col-md-12">

                                    <label class="radio-inline"><input {{$gender=='Male' ? 'checked' : '' }}  type="radio" value="Male" name="gender">Male</label>

                                    <label class="radio-inline"><input type="radio" value="Female" {{$gender=='Female' ? 'checked' : '' }}  name="gender">Female</label>
                                     @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif


                                </div>
                                
                            </div>
                            

                               
                                
                          
                            	<h4  style="margin-top:50px;" class="text-info">*Only if you want to change the password.*</h4>
                            	
							

							<div class="form-group {{ Session::get('incorrect') ? 'has-error' : ''}}">
								<label for="current_password">Current Password</label>
								<input type="password" class="form-control" id="current_password"
									   name="current_password">
									   @if (Session::get('incorrect'))
                                        <span class="help-block">
                                        <strong>{{Session::get('incorrect') }}</strong>
                                    </span>
                                    @endif
							</div>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">New Password
									<small></small>
								</label>
								<input type="password" class="form-control" id="password"
									   name="password">
									    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
							</div>
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<label for="password-confirm">Confirm Password
									<small></small>
								</label>
								<input type="password" class="form-control" id="password_confirmation"
									   name="password_confirmation">
									    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
							</div>

							

							<div class="form-group">
								<button class="btn btn-primary mr15" type="submit">Save Changes</button>
								<button class="btn btn-danger" type="button" id="cancelBtn"
										onclick="window.history.back()">Cancel
								</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>




@endsection

@push('scripts')
<script>
$('#profileImgBtn').on('click',function(){
    $('input[name="profileImg"]').click();
})

$('input[name="profileImg"]').on('change',function() {
 


  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imgPreview').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
  }
   $('#imgPreview').removeClass('hidden');

});


    var img = "{{$profileImg}}";
    if (img != "") {
        $('#imgPreview').removeClass('hidden');
    }
</script>



@endpush