 <script src="{{asset('bower_components/cropper/dist/cropper.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('bower_components/cropper/dist/cropper.min.css')}}">
<div class="modal" id="imageCropperModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit Image</h4>
            </div>
            <div class="modal-body">
                <img id="cropImgSrc" class="h400">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="saveCroppedImg">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#cropper_pic').on('change', function (e) {
        var imgBase = '';

        if (this.files && this.files[0]) {
            if (this.files[0].type.indexOf('image') >= 0) {

            } else {
                return alert('Invalid file format');
            }
            var FR = new FileReader();
            FR.onload = function (e) {
                imgBase = e.target.result;

                $('#imageCropperModal').modal('show');

                $('#cropImgSrc').attr('src', imgBase).cropper({
                    aspectRatio: aspectRatioInput,
                });
            };
            FR.readAsDataURL(this.files[0]);
        }
    });


    $('#imageCropperModal').on('hidden.bs.modal', function () {
        $('#cropImgSrc').cropper('destroy');
    });

    $('#saveCroppedImg').on('click', function () {
        var orgImageDetails = $('#cropImgSrc').cropper('getImageData');
        var height = orgImageDetails.naturalHeight;
        var width = orgImageDetails.naturalWidth;

        var exportResolution = {
            height: height,
            width: width
        };

        //console.log(exportResolution);

        if (width > 1280) {
            exportResolution = {
                height:640,
                width:1366
            };
        }

        //console.log(exportResolution);

        var croppedImgSrc = $('#cropImgSrc').cropper('getCroppedCanvas', exportResolution).toDataURL();
        $('#croppedImagePreview').attr('src', croppedImgSrc);
        $('#inputCropperPic').attr('value', croppedImgSrc);

        $('#imageCropperModal').modal('hide');
        $('#previewWrapper').removeClass('hidden');
    });


    $('#removeCroppedImage').on('click', function () {
        $('#croppedImagePreview').attr('src', '');
        $('#inputCropperPic').attr('value', '');
        $('#previewWrapper').addClass('hidden');
    });

</script>