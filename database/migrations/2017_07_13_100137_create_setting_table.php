<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('website_name')->nullable();
            $table->string('website_name_short')->nullable();
            $table->string('website_notice')->nullable();
            $table->string('fb_url')->nullable();
            $table->string('google_plus_url')->nullable();
            $table->string('skype_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->longText('fb_page')->nullable();
            $table->string('logo')->nullable();
            $table->string('slogan')->nullable();
            $table->string('short_description')->nullable();
            $table->longText('google_maps_iframe')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_email')->nullable();
            $table->bigInteger('owner_contact')->nullable();
            $table->string('owner_address')->nullable();
            $table->string('version')->nullable();
            $table->string('_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
